function filterSelection(c) {
  var x, i;
  const elemList = []

  x = document.getElementsByClassName("filterDiv");

  for (i = 0; i < x.length; i++) {
    if (x[i].classList.contains("fullImage")){
      x[i].classList.remove("fullImage");

      x[i].classList.add("projects__img2");

    }
    if (x[i].classList.contains("show")){
      x[i].classList.remove("show");
    }
  }

  if (c == "all") {
    for (i = 0; i < x.length; i++) {
      x[i].classList.add("show");
      
    }
  } else {
    for (i = 0; i < x.length; i++) {
      if (x[i].classList.contains(c)) {
        x[i].classList.add("show");
        elemList.push(x[i]);
      }
      else {
        x[i].classList.remove("show");
      }
    }
  }

  var previousElem = [];
  for (i = 0; i < elemList.length; i++){
    var len = elemList.length;
    if (i >= 1 && (i < len - 1) && elemList[i].classList[len-2] !== previousElem[1] && elemList[i].classList[len-2] !== elemList[i+1].classList[len-2]){
      elemList[i].classList.remove("projects__img2");
      elemList[i].classList.add("fullImage");
    }
    else if (i === (len-1) && ((elemList[i].classList.contains("projects__img2") && (elemList[i].classList[len-2] !== previousElem[1]))|| ((elemList[i].classList[len-2] === previousElem[1]) && previousElem[1]===previousElem[0]))){
      elemList[i].classList.remove("projects__img2");

      elemList[i].classList.add("fullImage");
    }
    else if (i === 0 && elemList[i].classList.contains("projects__img2") && elemList[i+1].classList.contains("projects__img")){
      elemList[i].classList.remove("projects__img2");
      elemList[i].classList.add("fullImage");
    }
    if (previousElem.length <= 1 ){
      previousElem.push(elemList[i].classList[len-2]);
    }
    else{
      previousElem.push(elemList[i].classList[len-2]);
      previousElem.splice(1, previousElem.length);
    }
  }
}


function openExample(){
  if (document.getElementById('containerExample').style.display !== "block"){
    document.getElementById('containerExample').style.display = 'block';
  }
  else{
    document.getElementById('containerExample').style.display = 'none';
  }
}  

function makeEnable(){
  var x = document.getElementById("serviceType").value;
  var values = Array.from(document.getElementById("serviceType").options).map(e => e.value);

  if (x !== 0 && values.includes(x)){
    document.getElementById('service').disabled = false;
    document.getElementById("serviceType").options[0].disabled = true;
  }
}

function makeBtnEnable(){
  var x = document.getElementById("service").value;
  var values = Array.from(document.getElementById("service").options).map(e => e.value);
  if (x !== 0 && values.includes(x)){
    document.getElementById('btnCalculator').disabled = false;
    document.getElementById("service").options[0].disabled = true;
  }
}


function showAnswer(c){
  var elem = document.getElementById(c);
  var allAnswer = document.getElementsByClassName("toggle");
  for (i=0; i< allAnswer.length; i++){
    if (allAnswer[i].checked){
      var answerId = allAnswer[i].id
      var question = document.getElementsByClassName(answerId);
      question[0].style.display = "none";
      var answerNum = answerId.slice(-1);
      document.getElementById('sign'+answerNum).style.transform = 'rotate(0deg)';
    }
  }
  var answerNo = c.slice(-1);
  if (elem.checked == true){
    document.getElementById('sign'+answerNo).style.transform = 'rotate(47deg)';
    var question = document.getElementsByClassName(c);
    question[0].style.display = "block";
  }
  else{
    document.getElementById('sign'+answerNo).style.transform = 'rotate(0deg)';
  }
}
